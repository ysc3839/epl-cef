#epl-cef

Easy Programming Language Chromium Embedded Framework

##文件说明
epl-cef.e            模块源码

epl-cef.ec           编译后的模块

cefsimple.e          翻译自C++示例代码中的cefsimple

epl-cef-subprocess.e cef子进程

cefsimple-window.e   使用易语言窗口的cefsimple

##使用说明
到 https://cefbuilds.com/ 或 http://pan.baidu.com/s/1o6Fn6gM 下载CEF Branch 2171 解压后即可测试
